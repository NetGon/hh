<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<title>
			hh.netgon.ru
		</title>
		<link rel="stylesheet" href="/assets/css/style.css">
	</head>
	<body>
		<header>
			<h1>
				Добро пожаловать в hh.netgon.ru
			</h1>
		</header>
		<main>
			<div class="home_wrapper">
				<a href="/user_in" class="__btn __btn-success">
					Регистрация
				</a>
				<a href="/log_in" class="__btn __btn-success">
					Войти
				</a>
				<a href="/generator_doc" class="__btn __btn-success">
					Генерация договоров
				</a>
			</div>
		</main>

	</body>
</html>
