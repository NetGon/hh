<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<title>
			hh.netgon.ru
		</title>
		<link rel="stylesheet" href="/assets/css/style.css">
	</head>
	<body>
		<header>
			<h1>
				Создать компанию
			</h1>
		</header>
		<main>
			<div class="home_wrapper">
				<form action="/api/create_company" class="__small-form">
					<label>
						Название компании
						<input type="text" id="title_company" />
					</label>
					<label>
						Email
						<input type="email" id="email_company" />
					</label>
					<button type="button" id="send_create_company" class="__btn __btn-success">
						Создать
					</button>
				</form>
				<div id="sendData" style="display: none">
					Ссылка: <a href="" id="linkInputCabinet"></a> <br />
					Пароль: <span id="pass"></span>
				</div>
			</div>
		</main>
		<script type="text/javascript" src="/assets/js/m.js"></script>
	</body>
</html>
