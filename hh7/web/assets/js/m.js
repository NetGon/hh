document.addEventListener("DOMContentLoaded", ready);

function ready() {
	var el = document.querySelector("#send_create_company");
	var idNewCookie = 0;
	if (el) {
		el.onclick = function (e) {
			e.preventDefault();
			if (!canCreateNewCompany(idNewCookie)) {
				alert('Wait 30 sec and try again.');
				return;
			}
			var title = document.querySelector("#title_company");
			var email = document.querySelector("#email_company");
			if (title.value && email.value) {
				request("/api/create_company", {
					title: title.value,
					email: email.value
				}, handlerRequestCreateCompany);
				setCookie('newCompany_' + idNewCookie++, 1, {
					expires: 30
				});
			} else {
				alert("Неверные данные");
			}
			return false;
		}
	}

	var el2 = document.querySelector("#inputPanel");
	if (el2) {
		el2.onclick = function (e) {
			e.preventDefault();
			var password = document.querySelector("#password");
			if (password.value) {
				var t = window.location.href.split('/');

				request("/api/input_panel", {
					company: t[t.length - 1],
					password: password.value,
				}, handlerRequestInputPanel)
			} else {
				alert("Неверные данные");
			}
			return false;
		}
	}

	var el3 = document.querySelector("#labelUpdateId");
	if (el3) {
		var t = window.location.href.split('/');
		request("/api/company_vac", {
			code: t[4]
		}, handlerRequestGetCompanyVac)
	}

	var el4 = document.querySelector("#createCompany");
	if (el4) {
		el4.onclick = function (e) {
			e.preventDefault();

			var idUpdate = document.querySelector('#idUpdate');
			var title = document.querySelector('#title');
			var body = document.querySelector('#body');
			var salary = document.querySelector('#salary');
			var stack = document.querySelector('#stack');
			var archive = document.querySelector('#archive');

			if (title.value && body.value && salary.value && stack.value && stack.value) {
				var t = window.location.href.split('/');
				request("/api/company_create", {
					code: t[t.length - 1],
					update: e.target.dataset.type,
					idUpdate: idUpdate.value,
					title: title.value,
					body: body.value,
					salary: salary.value,
					stack: stack.value,
					archive: archive.checked
				}, handlerRequestCreateCompanyVac);

				title.value = "";
				body.value = "";
				salary.value = "";
				stack.value = "";

				if (e.target.dataset.type == 1) {
					var labelUpdateId = document.querySelector('#labelUpdateId');
					labelUpdateId.style.display = "none";

					var createCompany = document.querySelector('#createCompany');
					createCompany.innerHTML = "Create new";
					createCompany.dataset.type = "0";
				}

			} else {
				alert("Bad data");
			}
			return false;
		}
	}

	var el5 = document.querySelector("#loginAdmin");
	if (el5) {
		el5.onclick = function (e) {
			e.preventDefault();
			var password = document.querySelector("#password");
			var email = document.querySelector("#email");
			if (password.value) {
				var t = window.location.href.split('/');

				request("/api/input_panel_admin", {
					email: email.value,
					password: password.value
				}, handlerRequestInputPanelAd)
			} else {
				alert("Неверные данные");
			}
			return false;
		}
	}

	var el6 = document.querySelector("#vacWrapAdmin");
	if (el6) {
		request("/api/get_all_company", {}, handlerRequestGetAllCompany)
	}

	var el7 = document.querySelector("#vacCompanyWrapAdmin");
	if (el7) {
		var t = window.location.href.split('/');
		request("/api/company_vac", {
			code: t[t.length - 1]
		}, handlerRequestGetAllVacCompany);
	}

	var el8 = document.getElementById('submit_logIn');
	var idSendPass = 0;
	if (el8) {
		el8.onclick = function(e) {
			e.preventDefault();
			if (!canSendPass(idSendPass)) {
				alert('Wait 30 sec and try again.');
				return;
			}
			var email = document.querySelector("#email_logIn");
			var pass = document.querySelector("#pass_logIn");
			if (pass.value && email.value) {
				request("/api/logining", {
					email: email.value,
					pass: pass.value
				}, handlerRequestLogIn);
				setCookie('sendPass_' + idSendPass++, 1, {
					expires: 30
				});
			} else {
				alert("Неверные данные");
			}
			return false;
		}
	}

	var el10 = document.getElementById('gen_mod');
	if (el10) {
		var onlyOneCollection = document.querySelectorAll('.only_one');
		el10.onchange = function(e) {
			if (+el10.value === 1) {
				[].forEach.call(onlyOneCollection, function(el) {
					el.style.display = 'block';
				});
			} else {
				[].forEach.call(onlyOneCollection, function(el) {
					el.style.display = 'none';
				});
			}
		}
	}

	var el11 = document.getElementById('peopleResume');
	if (el11) {
		var t = window.location.href.split('/');
		request('/api/peopleResume', {
			id: t[t.length - 1],
			code: t[t.length - 3],
			action: 'getAll'
		}, handlerRequestPeopleResume);
	}
}

function request(url, params, cb) {
	var xhr = new XMLHttpRequest();

	var body = "";
	for (var key in params) {
		if (body == "") {
			body += key + "=" + encodeURIComponent(params[key]);
		} else {
			body += "&" + key + "=" + encodeURIComponent(params[key]);
		}
	}

	xhr.open('POST', url, true);
	xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

	xhr.send(body);

	xhr.onreadystatechange = function() {
		if (this.readyState != 4) return;
		if (this.status != 200) {
			alert( 'ошибка: ' + (this.status ? this.statusText : 'запрос не удался') );
			return;
		}
		var data = {};
		if (this.responseText) {
			try {
				data = JSON.parse(this.responseText);
				console.log(data);
			} catch {
				console.log(this.responseText)
			}


			if (data.error) {
				return alert("Ошибка! " + data.error);
			} else if(cb) {
				return cb(data);
			}
		}
	}
}

function handlerRequestCreateCompany(data) {
	var sendData = document.querySelector("#sendData");
	var linkInputCabinet = document.querySelector("#linkInputCabinet");
	var pass = document.querySelector("#pass");
	sendData.style.display = "block";
	linkInputCabinet.href = "/panel/" + data.content.link;
	linkInputCabinet.innerHTML = "/panel/" + data.content.link;
	pass.innerHTML = data.content.id;
	return true;
}

function handlerRequestInputPanel(data) {
	window.location.href = "/list/" + data.content;
}

function handlerRequestGetCompanyVac(data) {
	var wrap = document.querySelector("#vacWrap");
	wrap.innerHTML = "";

	var vac = window.location.href.split('/');
	var idUser = vac[4];

	for (var key in data.content) {
		var t = document.createElement('tr');

		var td = document.createElement('td');
		var a = document.createElement('a');
		a.href = '/list/' + idUser + '/people/' + key;
		a.appendChild(td);
		td.innerHTML = key;
		t.append(a);

		var td = document.createElement('td');
		td.innerHTML = data.content[key].title;
		t.append(td);

		td = document.createElement('td');
		if (data.content[key].k != -1) {
			// td.innerHTML = "" + data.content[key].salary + "(" + (data.content[key].salary * data.content[key].k) + ") | " + data.content[key].k;
			td.innerHTML = "" + data.content[key].salary;
		} else {
			// td.innerHTML = "" + data.content[key].salary + "(" + (data.content[key].salary * data.k) + ") | " + data.k;
			td.innerHTML = "" + data.content[key].salary;
		}
		t.append(td);

		td = document.createElement('td');
		td.innerHTML = (data.content[key].archive === 'true' ? 'Да' : 'Нет');
		t.append(td);

		td = document.createElement('td');
		td.innerHTML = data.content[key].body;
		t.append(td);

		td = document.createElement('td');
		td.innerHTML = data.content[key].stack;
		t.append(td);

		td = document.createElement('td');

		var button = document.createElement('button');
		button.className = "__btn __btn-success";
		button.innerHTML = "Обновить";
		button.dataset.id = key;
		button.onclick = function (e) {
			e.preventDefault();
			toEditVac(e.target.dataset.id);
			return false;
		}

		td.append(button);
		t.append(td);

		wrap.append(t);
	}
}

function handlerRequestCreateCompanyVac(data) {
	var t = window.location.href.split('/');

	request("/api/company_vac", {
		code: t[t.length - 1]
	}, handlerRequestGetCompanyVac);
}

function toEditVac(id) {
	var t = window.location.href.split('/');

	request("/api/company_vac_by_id", {
		code: t[t.length - 1],
		id: id
	}, handlerRequestGetCompanyVacById);
}

function toEditField(id, where) {
	var val = document.querySelector("#k_" + id);
	if (where == 1) {
		var t = window.location.href.split('/');
		request("/api/change_k", {
			code: t[t.length - 1],
			id: id,
			where: where,
			val: val.value
		}, handlerRequestChangeK1);
	} else {
		request("/api/change_k", {
			code: id,
			where: where,
			val: val.value
		}, handlerRequestChangeK0);
	}
}

function handlerRequestGetCompanyVacById(data) {
	var idUpdate = document.querySelector('#idUpdate');
	var title = document.querySelector('#title');
	var body = document.querySelector('#body');
	var salary = document.querySelector('#salary');
	var stack = document.querySelector('#stack');
	var archive = document.querySelector('#archive');

	var labelUpdateId = document.querySelector('#labelUpdateId');
	labelUpdateId.style.display = "block";

	var createCompany = document.querySelector('#createCompany');
	createCompany.innerHTML = "Обновить";
	createCompany.dataset.type = "1";

	idUpdate.value = data.content.id;
	title.value = data.content.title;
	body.value = data.content.body;
	salary.value = data.content.salary;
	stack.value = data.content.stack;

	if (data.content.archive == "true") {
		archive.checked = true;
	} else {
		archive.checked = false;
	}
}

function handlerRequestInputPanelAd(data) {
	window.location.href = "/adminpanel/all-list";
}

function handlerRequestGetAllCompany(data) {
	var wrap = document.querySelector("#vacWrapAdmin");
	wrap.innerHTML = "";

	for (var key in data.content.pass) {
		var t = document.createElement('tr');

		var td = document.createElement('td');
		var a = document.createElement('a');
		a.innerHTML = data.content.pass[key].pass;
		a.href = "/adminpanel/" + key;
		td.append(a);
		t.append(td);

		var td = document.createElement('td');
		td.innerHTML = data.content.pass[key].company;
		t.append(td);

		var td = document.createElement('td');
		td.innerHTML = key;
		t.append(td);

		td = document.createElement('td');
		td.innerHTML = data.content.pass[key].email;
		t.append(td);

		td = document.createElement('td');
		if (data.content.pass[key].k != -1) {
			td.innerHTML = data.content.pass[key].k;
		} else {
			td.innerHTML = data.defaultK;
		}
		t.append(td);

		td = document.createElement('td');

		var button = document.createElement('button');
		button.className = "__btn __btn-success";
		button.innerHTML = "Обновить";
		button.dataset.id = key;
		button.dataset.where = 0;
		button.onclick = function (e) {
			e.preventDefault();
			toEditField(e.target.dataset.id, e.target.dataset.where);
			return false;
		}

		var input = document.createElement('input');
		input.placeholder = "k";
		input.id = "k_" + key;
		input.className = "__k-class";


		td.append(input);
		td.append(button);
		t.append(td);

		wrap.append(t);
	}
}

function handlerRequestGetAllVacCompany(data) {
	var wrap = document.querySelector("#vacCompanyWrapAdmin");
	wrap.innerHTML = "";
	var path = window.location.href.split('/');
	var idUser = path[4];

	for (var key in data.content) {
		var t = document.createElement('tr');

		var td = document.createElement('td');
		var a = document.createElement('a');
		a.href = '/adminList/' + idUser + '/people/' + key;
		a.appendChild(td);
		td.innerHTML = key;
		t.append(a);

		var td = document.createElement('td');
		td.innerHTML = data.content[key].title;
		t.append(td);

		var td = document.createElement('td');
		td.innerHTML = data.content[key].body;
		t.append(td);

		td = document.createElement('td');
		td.innerHTML = data.content[key].stack;
		t.append(td);

		var td = document.createElement('td');
		if (data.content[key].k != -1) {
			td.innerHTML = "" + data.content[key].salary + "(" + (data.content[key].salary * data.content[key].k) + ") | " + data.content[key].k;
		} else {
			td.innerHTML = "" + data.content[key].salary + "(" + (data.content[key].salary * data.k) + ") | " + data.k;
		}

		t.append(td);

		td = document.createElement('td');
		td.innerHTML = (data.content[key].archive === 'true' ? 'Да' : 'Нет');
		t.append(td);

		td = document.createElement('td');

		var button = document.createElement('button');
		button.className = "__btn __btn-success";
		button.innerHTML = "Обновить";
		button.dataset.id = key;
		button.dataset.where = 1;
		button.onclick = function (e) {
			e.preventDefault();
			toEditField(e.target.dataset.id, e.target.dataset.where);
			return false;
		}

		var input = document.createElement('input');
		input.placeholder = "k";
		input.id = "k_" + key;
		input.className = "__k-class";

		td.append(input);

		td.append(button);
		t.append(td);

		wrap.append(t);
	}
}

function handlerRequestChangeK1() {
	var t = window.location.href.split('/');
	request("/api/company_vac", {
		code: t[t.length - 1]
	}, handlerRequestGetAllVacCompany);
}

function handlerRequestChangeK0() {
	request("/api/get_all_company", {}, handlerRequestGetAllCompany)
}

function handlerRequestLogIn(data) {
	if (!data.error) {
		if (data.content == "admin") {
			handlerRequestInputPanelAd();
		} else {
			window.location.href = "/panel/" + data.content;
		}
	}
}

// function handlerRequestGeneratorDoc(data) {
// 	data
// }

function canCreateNewCompany(id) {
	return id < 3 || getCookie( 'newCompany_' + (id-3) ) === undefined;
}

function canSendPass(id) {
	return id < 5 || getCookie( 'sendPass_' + (id-5) ) === undefined;
}

function getCookie(name) {
  var matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

function setCookie(name, value, options) {
  options = options || {};

  var expires = options.expires;

  if (typeof expires == "number" && expires) {
    var d = new Date();
    d.setTime(d.getTime() + expires * 1000);
    expires = options.expires = d;
  }
  if (expires && expires.toUTCString) {
    options.expires = expires.toUTCString();
  }

  value = encodeURIComponent(value);

  var updatedCookie = name + "=" + value;

  for (var propName in options) {
    updatedCookie += "; " + propName;
    var propValue = options[propName];
    if (propValue !== true) {
      updatedCookie += "=" + propValue;
    }
  }

  document.cookie = updatedCookie;
}

function handlerRequestPeopleResume(data, admin = false) {
	var tb = document.getElementById('peopleResume');
	var t = window.location.href.split('/');

	if(t[3] === "adminList") {
		admin = true;
	}

	data.content.forEach(function(el, i) {
		var tr = document.createElement('tr');

		var td = document.createElement('td');
		td.innerHTML = i;
		tr.appendChild(td);

		var td = document.createElement('td');
		td.innerHTML = el.idVac;
		tr.appendChild(td);

		if (admin) {
			var td = document.createElement('td');
			td.innerHTML = defineStatus(el.status);
			tr.appendChild(td);
		} else {
			var td = document.createElement('td');
			for (var j = 1; j < 4; ++j) {
				td.appendChild(makeRadioButton(el.idVac, i, j, +j === +el.status));
			}
			tr.appendChild(td);
		}


		tb.appendChild(tr);
	});
}

function defineStatus(id) {
	switch (+id) {
		case 1:
			return 'Подходит';
		case 2:
			return 'На рассмотрении';
		case 3:
			return 'Не подходит';
		default:
			return 'Неизвестно';
	}
}

function makeRadioButton(idVac, name, status, active = false) {
	var label = document.createElement('label');
	var radio = document.createElement('input');
	radio.type = 'radio';
	radio.name = 'status_' + name;
	radio.style.width = 'auto';
	radio.style.display = 'inline';

	if (active) {
		radio.checked = true;
	}

	radio.addEventListener('click', (function(status, idVac) {
		return function(e) {
			var t = window.location.href.split('/');
			request('/api/peopleResume', {
				id: t[t.length - 1],
				code: t[t.length - 3],
				action: 'changeStatus',
				idVac: idVac,
				status: status
			});
		};
	})(status, idVac));

	label.appendChild(document.createTextNode(defineStatus(status)));
	label.appendChild(radio);
	return label;
}
