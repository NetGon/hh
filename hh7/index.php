<?php
		
	ini_set('error_reporting', E_ALL);
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);


	require_once __DIR__ . '/controllers/routing/Routing.php';

	$config = require_once __DIR__ . '/config/config.php';

	(new Routing($config))->run();