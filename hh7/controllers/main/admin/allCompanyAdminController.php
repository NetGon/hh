<?php
	
	require_once dirname(__DIR__, 2) . '/auth/checkAuthAdmin.php';
	require_once dirname(__DIR__, 2) . '/render/Render.php';


	$r = new Render();
	return $r->render('adminList', []);
