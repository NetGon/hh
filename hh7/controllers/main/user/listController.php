<?php

	require_once dirname(__DIR__, 2) . '/render/Render.php';
	$r = new Render();

	$arr = explode("/", $_SERVER['REQUEST_URI']);
	$code = $arr[2];

	$secret = "jnadaus8hasdbayus32lmsfn23fadsf3411rwf";

	if (isset($_COOKIE[$code])) {
		$md5data = md5($code . $secret);
		if (htmlspecialchars($_COOKIE[$code]) == $md5data) {
			if (count($arr) > 3 && $arr[3] == 'people') {
				$r->render('people');
			} else {
				$dataToRender = getAllDataByCode($code);
				return $r->render('list', $dataToRender);
			}

		}
	} else {
		header('Location: /panel/' . $code);
		return;
	}

	function getAllDataByCode($code) {
		$jsonPass = file_get_contents(dirname(__DIR__, 3) . '/web/json/pass.json');
		$jsonPassDecode = json_decode($jsonPass);

		if ($jsonPassDecode->pass->{$code}) {

			$company = $jsonPassDecode->pass->{$code}->company;
			$link = $code;
			$id = $jsonPassDecode->pass->{$code}->pass;
			$k = $jsonPassDecode->pass->{$code}->k;
			if (isset($jsonPassDecode->pass->{$code}->salary)) {
				$salary = $jsonPassDecode->pass->{$code}->salary;
			}

			$defaulKoefFile = file_get_contents(dirname(__DIR__, 3) . '/web/json/admin/salary.json');
			$defaultK = json_decode($defaulKoefFile);
			$defaultK = $defaultK->default_salary;

			return [
				'company' => $company,
				'link' => $link,
				'id' => $id,
				'k' => $k == -1 ? $defaultK : $k
			];
		} else {
			return null;
		}
	}
