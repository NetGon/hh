<?php
require 'vendor/autoload.php';
require_once dirname(__DIR__, 3) . '/vendor/dompdf/dompdf/src/Dompdf.php';

use Dompdf\Dompdf;

// $data = $_COOKIE['file'];
// $data = json_decode($data, true);


$data = $_POST;
$fullName = $data['surname'] . $data['name'] . $data['patronymic'];

$data = array_merge($data, [
    "day" => date('d'),
    "month" => date('m'),
    "year" => date('Y'),
    "dateNumber" => date('Y'),
    "fullName" => $fullName
]);

$jsonUser = file_get_contents(dirname(__DIR__, 3) . '/web/doc/test_user.json');
$user = json_decode($jsonUser);

$data = array_merge($data, [
    "json_fullName" => $user->fullName,
    "json_passport" => $user->passport,
    "json_address" => $user->address,
    "json_requisites" => $user->requisites,
    "json_INN" => $user->INN,
    "json_settlementAccount" => $user->settlementAccount,
    "json_correspondentAccount" => $user->correspondentAccount,
    "json_BIK" => $user->BIK
]);

require_once dirname(__DIR__, 2) . '/render/Render.php';

$mod = $data['mod'];

$r = new Render('/web/templates');
$html = $r->render('doc' . $mod, $data, false);

$dompdf = new Dompdf([]);

$dompdf->set_option('fontDir', dirname(__DIR__, 3) . '/vendor/dompdf/dompdf/lib/fonts');
$dompdf->set_option('defaultFont', 'arialuni');

$dompdf->loadHtml($html, 'UTF-8');

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A4');

// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
$dompdf->stream("doc.pdf");

?>
