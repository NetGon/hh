<?php

    $data = (Object)[];
    $data->error = null;

    $code = $_POST['code'];
    $numRes = $_POST['id'];

    $jsonPass = file_get_contents(dirname(__DIR__, 3) . '/web/json/pass.json');
	$jsonPassDecode = json_decode($jsonPass);
    $company = $jsonPassDecode->pass->{$code}->company;
    $link = $code;
    $id = $jsonPassDecode->pass->{$code}->pass;
    $folder = $company . "_" . $id . "_" . $link;
    $resume = dirname(__DIR__, 3) . '/web/json/company/' . $folder . "/resume.json";

    if ($_POST['action'] == 'getAll') {
        $files = scandir(dirname(__DIR__, 3) . '/web/json/company/' . $folder . '/resume/' . $numRes);
        $files = array_slice($files, 2);

        $needjson = [];
        if (file_exists($resume)) {
            $jsonList = file_get_contents($resume);
            $json = json_decode($jsonList, true);


            foreach ($json as $key => $note) {
                if ($note['folder'] == $numRes) {
                    $fileIsset = false;
                    foreach ($files as $keyFile => $value) {
                        if ($value == $json[$key]['idVac']) {
                            $fileIsset = true;
                            unset($files[$keyFile]);
                            array_push($needjson, $note);
                            break;
                        }
                    }

                    if (!$fileIsset) {
                        unset($json[$key]);
                    }
                }
            }
        } else {
            $json = [];
        }


        for ($i = 0; $i < count($files); $i++) {
            $el = [];
            $el['status'] = 3;
            $el['idVac'] = $files[$i];
            $el['folder'] = $numRes;

            array_push($json, $el);
            array_push($needjson, $el);
        }

        file_put_contents($resume, json_encode($json));
        $data->content = $needjson;
    } else if ($_POST['action'] == 'changeStatus') {
        $idVac = $_POST['idVac'];

        $jsonList = file_get_contents($resume);
        $json = json_decode($jsonList, true);

        foreach ($json as $key => $value) {
            if ($json[$key]['folder'] == $numRes && $json[$key]['idVac'] == $idVac) {
                $json[$key]['status'] = $_POST['status'];
            }
        }

        file_put_contents($resume, json_encode($json));
    }

    echo json_encode($data);
?>
