<?php
	
	require_once dirname(__DIR__, 2) . '/auth/checkAuthAdmin.php';

	$data = (Object)[];
	if (!isset($_POST['code'])) {
		$data->error = "Bad params";
		echo json_encode($data);
		return;
	}

	$jsonPass = file_get_contents(dirname(__DIR__, 3) . '/web/json/pass.json');
	$jsonPassDecode = json_decode($jsonPass);

	$code = $_POST['code'];

	if ($_POST['where'] == 0) {
		if ($jsonPassDecode->pass->{$code}) {
			$jsonPassDecode->pass->{$code}->k = $_POST['val'];

			file_put_contents(dirname(__DIR__, 3) . '/web/json/pass.json', json_encode($jsonPassDecode));

			$data->error = null;
		} else {
			$data->error = "Bad params";
		}
	} else if ($_POST['where'] == 1) {
		$company = $jsonPassDecode->pass->{$code}->company;
		$link = $code;
		$id = $jsonPassDecode->pass->{$code}->pass;

		$folder = $company . "_" . $id . "_" . $link;

		if (file_exists(dirname(__DIR__, 3) . '/web/json/company/' . $folder)) {
			if (file_exists(dirname(__DIR__, 3) . '/web/json/company/' . $folder . "/list.json")) {
				$jsonList = file_get_contents(dirname(__DIR__, 3) . '/web/json/company/' . $folder . "/list.json");
				$list = json_decode($jsonList);
				$list->{$_POST['id']}->k = $_POST['val'];

				file_put_contents(dirname(__DIR__, 3) . '/web/json/company/' . $folder . "/list.json", json_encode($list));
			}
		}
	}

	echo json_encode($data);

