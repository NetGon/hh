<?php

	$data = (Object)[];
	if (!isset($_POST['code'])) {
		$data->error = "Bad params";
		echo json_encode($data);
		return;
	}

	$jsonPass = file_get_contents(dirname(__DIR__, 3) . '/web/json/pass.json');
	$jsonPassDecode = json_decode($jsonPass);

	$code = $_POST['code'];

	if ($jsonPassDecode->pass->{$code}) {
		$company = $jsonPassDecode->pass->{$code}->company;
		$link = $code;
		$id = $jsonPassDecode->pass->{$code}->pass;

		$folder = $company . "_" . $id . "_" . $link;
		$list = [];

		$defaultK = -1;
		if ($jsonPassDecode->pass->{$code}->k != -1) {
			$defaultK = $jsonPassDecode->pass->{$code}->k;
		} else {
			$defaulKoefFile = file_get_contents(dirname(__DIR__, 3) . '/web/json/admin/salary.json');
			$defaultK = json_decode($defaulKoefFile);
			$defaultK = $defaultK->default_salary;
		}

		$data->defaultK = $defaultK;
		if (file_exists(dirname(__DIR__, 3) . '/web/json/company/' . $folder)) {
			if (file_exists(dirname(__DIR__, 3) . '/web/json/company/' . $folder . "/list.json")) {
				$jsonList = file_get_contents(dirname(__DIR__, 3) . '/web/json/company/' . $folder . "/list.json");
				$list = json_decode($jsonList);
				if (isset($list->{$_POST['id']})) {
					$data->content = $list->{$_POST['id']};
					$data->content->id = $_POST['id'];
					$data->error = null;
				} else {
					$data->error = "BAD data";
				}

			}
		}
	} else {
		$data->error = "Bad code";
	}

	echo json_encode($data);