<?php

	$data = (Object)[];
	if (!isset($_POST['title'])) {
		$data->error = "Bad params";
		echo $data;
		return;
	}

	$data->error = null;

	$jsonFile = file_get_contents(dirname(__DIR__, 3) . '/web/json/id.json');
	$jsonPass = file_get_contents(dirname(__DIR__, 3) . '/web/json/pass.json');
	$json = json_decode($jsonFile);
	$jsonPassDecode = json_decode($jsonPass);

 	if (isset($jsonPassDecode->pass)) {
		foreach ($jsonPassDecode->pass as $key => $value) {
			if (isset($value->email) && $value->email == $_POST['email']) {
				$data->error = "Емейл уже зарегистрирован";
				echo json_encode($data);
				return;
			}
		}
	}

	$str = 'qwertyuiopasdfghjklzxcvbnm1234567890';
	$shuffled = str_shuffle($str);
	$link = substr($shuffled, 0, 4);
	$folder = $_POST['title'] . "_" . $json->id . "_" . $link;

	if (!file_exists(dirname(__DIR__, 3) . '/web/json/' . $folder)) {
		mkdir(dirname(__DIR__, 3) . '/web/json/company/' . $folder);
		mkdir(dirname(__DIR__, 3) . '/web/json/company/' . $folder . '/resume');
	} else {
		$shuffled = str_shuffle($str);
		$link = substr($shuffled, 0, 4);
		$folder = $_POST['title'] . "_" . $json->id . "_" . $link;
		mkdir(dirname(__DIR__, 3) . '/web/json/company/' . $folder);
		mkdir(dirname(__DIR__, 3) . '/web/json/company/' . $folder . '/resume');
	}

	$ob = [];

	$jsonPassDecode->pass->{$link} = [
		'company' => $_POST['title'],
		'pass' => $json->id,
		'email' => $_POST['email'],
		'salary' => -1,
		'k' => -1
	];

	$data->content = [
		"id" => $json->id,
		"link" => $link
	];

	echo json_encode($data);

	$json->id = ($json->id + 1);

	file_put_contents(dirname(__DIR__, 3) . '/web/json/id.json', json_encode($json));
	file_put_contents(dirname(__DIR__, 3) . '/web/json/pass.json', json_encode($jsonPassDecode));

	return;
