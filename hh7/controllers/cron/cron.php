<?php

	$arrFolders = scandir(dirname(__DIR__, 2) . '/web/json/company');


	$jsonNew = [];

	foreach ($arrFolders as $key => $value) {
		if (strrpos($value, "_") != false) {

			$dataText = explode("_", $value);

			if (!file_exists(dirname(__DIR__, 2) . '/web/json/company/' . $value . "/list.json")) {
				continue;
			}

			$dataVac = file_get_contents(dirname(__DIR__, 2) . '/web/json/company/' . $value . "/list.json");
			$jsonVac = json_decode($dataVac);

			$jsonNew[$dataText[0]] = $jsonVac;
		}
	}

	file_put_contents(dirname(__DIR__, 2) . '/web/json/cronjson/all.json', json_encode($jsonNew));

