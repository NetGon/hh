<?php

/**
 * class Routing
 */
class Routing {
	function __construct($config) {}

	public $url = [
		'host' => '',
		'scheme' => '',
		'redirect_url' => '',
		'request_uri' => '',
		'method' => '',
	];

	private function createUrlObject($server) {
		
		$this->url['host'] = $server['HTTP_HOST'];
		$this->url['scheme'] = $server['REQUEST_SCHEME'];
		$this->url['redirect_url'] = isset($server['REDIRECT_URL']) ? $server['REDIRECT_URL'] : null;
		$this->url['request_uri'] = $server['REQUEST_URI'];
		$this->url['method'] = $server['REQUEST_METHOD'];

		$partsUrl = parse_url("http://" . $this->url['host'] . $this->url['request_uri']);

		if (isset($partsUrl['query'])) {
			parse_str($partsUrl['query'], $this->url['params']);
		} else {
			$this->url['params'] = [];
		}

		return true;
	}

	private function getUserRoute($url) {
		$route = require_once __DIR__ . '/route.php';

		if (isset($route[$url])) {
			return $route[$url]['controller'];
		}

		if (substr($url, -1) == '/') {
			$tempUrl = substr($url, 0, -1);
			if (isset($route[$tempUrl])) {
				return $route[$tempUrl]['controller'];
			}
		}

		$arr = explode("/", $url);
		
		$str = "";
		foreach ($arr as $index => $item) {
			if ($item == "") continue;
			$str .= "/" . $item . "/*";
			if (isset($route[$str])) {
				return $route[$str]['controller'];
			}
		}
		
		return null;
	}

	private function routingByUrl($url) {
		$controller = $this->getUserRoute($url);
		if ($controller == "" || $controller == null) {
			throw new Exception("Bad URL", 1);
		}

		require_once dirname(__DIR__, 2) . $controller;

		return true;
	}

	public function run() {
		$this->createUrlObject($_SERVER);

		$this->routingByUrl($this->url['request_uri']);

		return true;
	}
}