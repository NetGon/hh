<?php

	$jsonPass = file_get_contents(dirname(dirname(dirname(dirname(__FILE__)))) . '/web/json/pass.json');
	$jsonPassDecode = json_decode($jsonPass);

	$defaulKoefFile = file_get_contents(dirname(dirname(dirname(dirname(__FILE__)))) . '/web/json/admin/salary.json');
	$defaultK = json_decode($defaulKoefFile);
	$defaultK = $defaultK->default_salary;
	
	$ob = (Object)[];
	$ob->content = $jsonPassDecode;
	$ob->defaultK = $defaultK;
	
	echo json_encode($ob);