<?php

	$data = (Object)[];
	if (!isset($_POST['email'])) {
		$data->error = "Bad params";
		echo json_encode($data);
		return;
	}

	$jsonPass = file_get_contents(dirname(dirname(dirname(dirname(__FILE__)))) . '/web/json/pass.json');
	$jsonPassDecode = json_decode($jsonPass)->pass;

	$email = $_POST['email'];
	$pass = $_POST['pass'];

	if ($pass == "1" && $email == 'netgon@netgon.ru') {
		$secret = "jnadaus8hasdbayus32lmsfn23fadsf3411rwf";
		$md5data = md5("qwer1234asfSdqxAADMIN" . $secret);

		setcookie("netgonadmin", $md5data, time() + 3600, '/');
		$data->error = null;
		$data->content = "admin";
		echo json_encode($data);
		return;
	}

	$i = 0;
	foreach ($jsonPassDecode as $key => $user) {
		if ($user->email == $email && $user->pass == $pass) {
			$secret = "jnadaus8hasdbayus32lmsfn23fadsf3411rwf";
			$md5data = md5($key . $secret);

			setcookie($key, $md5data, time() + 3600, '/');

			$data->error = null;
			$data->content = $key;

			break;
		}
	}

	if (!isset($data->content)) {
		$data->error = "Bad password or email";
	}

	echo json_encode($data);
