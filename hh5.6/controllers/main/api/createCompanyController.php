<?php

	$data = (Object)[];
	if (!isset($_POST['code'])) {
		$data->error = "Bad params";
		echo json_encode($data);
		return;
	}

	$jsonPass = file_get_contents(dirname(dirname(dirname(dirname(__FILE__)))) . '/web/json/pass.json');
	$jsonPassDecode = json_decode($jsonPass);

	$code = $_POST['code'];

	if ($jsonPassDecode->pass->{$code}) {
		$company = $jsonPassDecode->pass->{$code}->company;
		$link = $code;
		$id = $jsonPassDecode->pass->{$code}->pass;

		$folder = $company . "_" . $id . "_" . $link;
		$list = [];

		if (file_exists(dirname(dirname(dirname(dirname(__FILE__))) . '/web/json/company/' . $folder))) {
			if (file_exists(dirname(dirname(dirname(dirname(__FILE__))) . '/web/json/company/' . $folder . "/list.json"))) {
				$jsonList = file_get_contents(dirname(dirname(dirname(dirname(__FILE__)))) . '/web/json/company/' . $folder . "/list.json");

				$list = json_decode($jsonList);
				$admin_salary = -1;
				if ($_POST['update'] == 0) {
					$id = count((array)$list);
				} else {
					$id =  $_POST['idUpdate'];
					$admin_salary = $list->{$id}->k;
				}

				$list->{$id} = [
					"title" => $_POST['title'],
					"body" => $_POST['body'],
					"salary" => $_POST['salary'],
					"k" => $admin_salary,
					"stack" => $_POST['stack'],
					"archive" => $_POST['archive']
				];

				file_put_contents(dirname(dirname(dirname(dirname(__FILE__)))) . '/web/json/company/' . $folder . "/list.json", json_encode($list));
			} else {
				$dataToJSON = (Object)[
					"0" => [
						"title" => $_POST['title'],
						"body" => $_POST['body'],
						"salary" => $_POST['salary'],
						"k" => "-1",
						"stack" => $_POST['stack'],
						"archive" => $_POST['archive']
					]
				];
				$id = 0;

				file_put_contents(dirname(dirname(dirname(dirname(__FILE__)))) . '/web/json/company/' . $folder . "/list.json", json_encode($dataToJSON));
			}

			$folder = dirname(dirname(dirname(dirname(__FILE__)))) . '/web/json/company/' . $folder . '/resume/' . $id . '/';
			mkdir($folder);

			$fp = fopen($folder . 'alex_' . $link . '.html', "w");
			fwrite($fp, "alex");
    		fclose($fp);

			$fp = fopen($folder . 'alex2_' . $link . '.html', "w");
			fwrite($fp, "alex");
    		fclose($fp);
		}

		$data->error = null;
		$data->content = true;
	} else {
		$data->error = "Bad code";
	}

	echo json_encode($data);
