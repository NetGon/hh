<?php

	$data = (Object)[];
	if (!isset($_POST['company'])) {
		$data->error = "Bad params";
		echo json_encode($data);
		return;
	}

	$jsonPass = file_get_contents(dirname(dirname(dirname(dirname(__FILE__)))) . '/web/json/pass.json');
	$jsonPassDecode = json_decode($jsonPass);

	$company = $_POST['company'];
	$password = $_POST['password'];

	if ($jsonPassDecode->pass->{$company}) {
		if ($jsonPassDecode->pass->{$company}->pass == $password) {
			$secret = "jnadaus8hasdbayus32lmsfn23fadsf3411rwf";
			$md5data = md5($company . $secret);

			setcookie($company, $md5data, time() + 3600, '/');

			$data->error = null;
			$data->content = $_POST['company'];
		} else {
			$data->error = "Bad password";
		}
	} else {
		$data->error = "Bad company";
	}

	echo json_encode($data);
