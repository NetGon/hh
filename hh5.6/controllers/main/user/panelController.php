<?php
	
	require_once dirname(dirname(dirname(__FILE__))) . '/render/Render.php';
	$r = new Render();

	$arr = explode("/", $_SERVER['REQUEST_URI']);
	$code = array_pop($arr);

	$secret = "jnadaus8hasdbayus32lmsfn23fadsf3411rwf";

	if (isset($_COOKIE[$code])) {
		$md5data = md5($code . $secret);
		if (htmlspecialchars($_COOKIE[$code]) == $md5data) {
			header('Location: /list/' . $code);
		}
	} else {
		return $r->render('panelInput', []);
	}
