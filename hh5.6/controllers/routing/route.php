<?php

	$route = [
		'/' => [
			'controller' => '/controllers/main/index/indexController.php',
		],

		'/user_in' => [
			'controller' => '/controllers/main/user/userController.php',
		],

		'/log_in' => [
			'controller' => '/controllers/main/user/logInController.php',
		],

		'/generator_doc' => [
			'controller' => '/controllers/main/user/generator_docController.php',
		],

		'/api/logining' => [
			'controller' => '/controllers/main/api/loginingController.php'
		],

		'/admin_in' => [
			'controller' => '/controllers/main/user/adminController.php',
		],

		'/api/create_company' => [
			'controller' => '/controllers/main/api/apiController.php',
		],

		'/api/generator_doc' => [
			'controller' => '/controllers/main/api/generator_docController.php',
		],

		'/panel/*' => [
			'controller' => '/controllers/main/user/panelController.php',
		],

		'/list/*' => [
			'controller' => '/controllers/main/user/listController.php',
		],

		'/api/input_panel' => [
			'controller' => '/controllers/main/api/inputController.php',
		],

		'/api/company_vac' => [
			'controller' => '/controllers/main/api/companyController.php',
		],

		'/api/company_create' => [
			'controller' => '/controllers/main/api/createCompanyController.php',
		],

		'/api/company_vac_by_id' => [
			'controller' => '/controllers/main/api/getCompanyVacByIdController.php',
		],

		'/api/input_panel_admin' => [
			'controller' => '/controllers/main/api/authAdminController.php',
		],

		'/adminpanel/all-list' => [
			'controller' => '/controllers/main/admin/allCompanyAdminController.php',
		],

		'/api/get_all_company' => [
			'controller' => '/controllers/main/api/getAllCompanyController.php',
		],

		'/adminpanel/*' => [
			'controller' => '/controllers/main/admin/companyDataController.php',
		],

		'/api/change_k' => [
			'controller' => '/controllers/main/api/changeKController.php',
		],

		'/api/generator_docController' => [
			'controller' => '/controllers/main/api/generator_docController.php'
		],

		'/api/peopleResume' => [
			'controller' => '/controllers/main/api/peopleResumeController.php'
		],

		'/adminList/*' => [
			'controller' => '/controllers/main/admin/listPeopleController.php',
		],

		'/test' => [
			'controller' => '/controllers/cron/cron.php',
		]
	];

	return $route;
