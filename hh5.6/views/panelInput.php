<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<title>
			hh.netgon.ru
		</title>
		<link rel="stylesheet" href="/assets/css/style.css">
	</head>
	<body>
		<header>
			<h1>
				Вход для пользователя
			</h1>
		</header>
		<main>
			<div class="home_wrapper">
				<form action="" class="__small-form">
					<label>
						Пароль
						<input type="password" id="password" />
					</label>
					<button type="button" id="inputPanel" class="__btn __btn-success">
						Войти
					</button>
				</form>
			</div>
		</main>
		<script type="text/javascript" src="/assets/js/m.js"></script>
	</body>
</html>
