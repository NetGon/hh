<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<title>
			hh.netgon.ru
		</title>
		<link rel="stylesheet" href="/assets/css/style.css">
	</head>
	<body>
		<header>
			<h1>
				Сгенерировать договор
			</h1>
		</header>
		<main>
			<div class="home_wrapper">
				<form action="/api/generator_docController" method="POST" class="__small-form">
                    <label>
                        <select id="gen_mod" name="mod">
                            <option value="1">Дополнительное соглашение &#8470;1</option>
                            <option value="2">Агентский договор &#8470;1</option>
                        </select>
                    </label>
                    <label>
                        Фамилия
                        <input type="text" name="surname" id="gen_surname" placeholder="Иванов" />
                    </label>
					<label>
						Имя
						<input type="text" name="name" id="gen_name" placeholder="Иван"/>
					</label>
					<label>
						Отчество
						<input type="text" name="patronymic" id="gen_patronymic" placeholder="Иваныч" />
					</label>
					<label>
						Гражданство
						<input type="text" name="citizenship" id="gen_citizenship" placeholder="Россия" />
					</label>
					<label>
						Паспорт/серия
						<input type="text" name="passport" id="gen_passport" placeholder="0000 000000" />
					</label>
					<label class="only_one">
						Заказчик
						<input type="text" name="customer" id="gen_customer" placeholder="" />
					</label>
					<label class="only_one">
						Цена
						<input type="text" name="price" id="gen_price" placeholder="20 рублей"/>
					</label>
					<label class="only_one">
						Вознаграждение Агента
						<input type="text" name="percent" id="gen_percent" placeholder="30 рублей" />
					</label>
					<label>
						Адрес
						<input type="text" name="address" id="gen_address" placeholder="ул. им. Ленина, дом 5, кв. 5"/>
					</label>
					<label>
						Реквизиты
						<input type="text" name="requisites" id="gen_requisites" placeholder=""/>
					</label>
					<label>
						ИНН
						<input type="text" name="INN" id="gen_INN" placeholder="000000000000" />
					</label>
					<label>
						Р/с
						<input type="text" name="settlementAccount" id="gen_settlementAccount" placeholder="00000000000000000000"/>
					</label>
					<label>
						Кор/счет
						<input type="text" name="correspondentAccount" id="gen_correspondentAccount" placeholder="00000000000000000000"/>
					</label>
					<label>
						Бик
						<input type="text" name="BIK" id="gen_BIK" placeholder="000000000"/>
					</label>

					<input type="text" value="active" name="active" id="active" style="display: none" />
					<input type="text" value="agent" name="agent" id="agent" style="display: none" />
					<input type="text" value="priceWords" name="priceWords" id="priceWords" style="display: none" />
					<input type="text" value="period" name="period" id="period" style="display: none" />
					<input type="text" value="otherPay" name="otherPay" id="otherPay" style="display: none" />

					<button type="submit" id="submit_generateDoc" value="Submit" class="__btn __btn-success">
						Сгенерировать
					</button>
				</form>
			</div>
		</main>
		<script type="text/javascript" src="/assets/js/m.js"></script>
	</body>
</html>
