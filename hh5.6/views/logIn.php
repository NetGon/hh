<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<title>
			hh.netgon.ru
		</title>
		<link rel="stylesheet" href="/assets/css/style.css">
	</head>
	<body>
		<header>
			<h1>
				Войти
			</h1>
		</header>
		<main>
			<div class="home_wrapper">
				<form action="/api/log_in" class="__small-form">
                    <label>
                        Email
                        <input type="email" id="email_logIn" />
                    </label>
					<label>
						Пароль
						<input type="password" id="pass_logIn" />
					</label>
					<button type="button" id="submit_logIn" class="__btn __btn-success">
						Войти
					</button>
				</form>
			</div>
		</main>
		<script type="text/javascript" src="/assets/js/m.js"></script>
	</body>
</html>
