<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<title>
			hh.netgon.ru
		</title>
		<link rel="stylesheet" href="/assets/css/style.css">
	</head>
	<body>
		<header>
			<h1>
				Список вакансий
			</h1>
		</header>
		<main>
			<div class="home_wrapper">
				<table class="home_wrapper__list-vac" width="100%" style="margin-top: 1em; margin-bottom: 2em;">
					<thead>
						<tr>
							<th>Id</th>
							<th>Название работы</th>
							<th>Описание</th>
							<th>Стек</th>
							<th>Оклад</th>
							<th>Архив</th>
							<th>Обновить</th>
						</tr>
					</thead>
					<tbody id="vacCompanyWrapAdmin">

					</tbody>
				</table>
			</div>
		</main>
		<script type="text/javascript" src="/assets/js/m.js"></script>
	</body>
</html>
