<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<title>
			hh.netgon.ru
		</title>
		<link rel="stylesheet" href="/assets/css/style.css">
	</head>
	<body>
		<header>
			<h1>
				Вход для админа
			</h1>
		</header>
		<main>
			<div class="home_wrapper">
				<form action="/api/create_company" class="__small-form">
					<label>
						email
						<input type="text" id="email" />
					</label>
					<label>
						Пароль
						<input type="text" id="password" />
					</label>
					<button type="button" id="loginAdmin" class="__btn __btn-success">
						Войти
					</button>
				</form>
			</div>
		</main>
		<script type="text/javascript" src="/assets/js/m.js"></script>
	</body>
</html>
