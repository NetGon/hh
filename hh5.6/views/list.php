<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<title>
			hh.netgon.ru
		</title>
		<link rel="stylesheet" href="/assets/css/style.css">
	</head>
	<body>
		<header>
			<h1>
				Добавление/Изменение
			</h1>
		</header>
		<main>
			<div class="home_wrapper">
				<h3>Данные компании:</h3>
				<div class="home_wrapper__company-data __small-form">
					<label>
						Название компании <input type="text" name="title" value="%%company%%" disabled />
					</label>
					<label>
						Ссылка <input type="text" name="title" value="%%link%%" disabled />
					</label>
					<label>
						Id <input type="text" name="title" value="%%id%%" disabled />
					</label>
					<!-- <label>
						k <input type="text" name="k" value="%%k%%" disabled />
					</label> -->
				</div>
				<h3>
					Добавление
				</h3>
				<form action="/api/create_company" class="__small-form">
					<label id="labelUpdateId" style="display: none;">
						id update <input type="text" name="idUpdate" id="idUpdate" disabled />
					</label>
					<label>
						Название работы <input type="text" name="title" id="title" />
					</label>
					<label>
						Описание работы <input type="text" name="body" id="body" />
					</label>
					<label>
						Оклад <input type="number" name="salary" id="salary" />
					</label>
					<label>
						Стек <input type="text" name="stack" id="stack" />
					</label>
					<label>
						Архив <input type="checkbox" name="archive" value="on" id="archive" />
					</label>
					<button type="button" id="createCompany" data-type="0" class="__btn __btn-success">
						Создать
					</button>
				</form>
				<table class="home_wrapper__list-vac" width="100%" style="margin-top: 1em; margin-bottom: 2em;">
					<thead>
						<tr>
							<th>Id</th>
							<th>Название</th>
							<th>Оклад</th>
							<th>Архив</th>
							<th>Описание</th>
							<th>Стек</th>
							<th>Обновить</th>
						</tr>
					</thead>
					<tbody id="vacWrap">

					</tbody>
				</table>
			</div>
		</main>
		<script type="text/javascript" src="/assets/js/m.js"></script>
	</body>
</html>
